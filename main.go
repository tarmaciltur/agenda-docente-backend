package main

import (
	"log"
	"os"
	"os/signal"

	"gitlab.com/softwareperonista/agenda-docente-backend/internal/data"
	"gitlab.com/softwareperonista/agenda-docente-backend/internal/server"
	"gitlab.com/softwareperonista/agenda-docente-backend/pkg/checkenv"

	_ "github.com/joho/godotenv/autoload"
)

func main() {
	port, err := checkenv.Checkempty("PORT")
	if err != nil {
		log.Fatal(err)
	}

	serv, err := server.New(port)
	if err != nil {
		log.Fatal(err)
	}

	d := data.New()
	if err := d.DB.Ping(); err != nil {
		log.Fatal(err)
	}

	go serv.Start()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c

	serv.Close()
}
