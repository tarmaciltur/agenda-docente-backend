package claim

import (
	"errors"

	"github.com/dgrijalva/jwt-go"
)

type Claim struct {
	jwt.StandardClaims
	Id int `json:"id"`
}

func (c *Claim) GetToken(signingString string) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, c)
	return token.SignedString([]byte(signingString))
}

func GetFromToken(tokemString, signingString string) (*Claim, error) {
	token, err := jwt.Parse(tokemString, func(*jwt.Token) (interface{}, error) {
		return []byte(signingString), nil
	})
	if err != nil {
		return nil, err
	}

	if !token.Valid {
		return nil, errors.New("invalid token")
	}

	claim, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		return nil, errors.New("invalid claim")
	}

	iId, ok := claim["id"]
	if !ok {
		return nil, errors.New("user id not found")
	}

	id, ok := iId.(float64)
	if !ok {
		return nil, errors.New("invalid user id")
	}

	return &Claim{Id: int(id)}, nil
}
