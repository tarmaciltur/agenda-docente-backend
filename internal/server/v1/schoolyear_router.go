package v1

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/go-chi/chi"
	"gitlab.com/softwareperonista/agenda-docente-backend/internal/middleware"
	"gitlab.com/softwareperonista/agenda-docente-backend/internal/models"
	"gitlab.com/softwareperonista/agenda-docente-backend/internal/server/response"
)

type SchoolYearRouter struct {
	Repository models.SchoolYearRespository
}

const SchoolYearPath string = "/schoolyear"

func (syr SchoolYearRouter) Routes() http.Handler {
	r := chi.NewRouter()

	r.With(middleware.Authorizator).Get("/", syr.GetAllHandler)
	r.With(middleware.Authorizator).Get("/{id}", syr.GetOneHandler)

	return r
}

func (syr SchoolYearRouter) GetAllHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	schoolyears, err := syr.Repository.GetAll(ctx)
	if err != nil {
		response.HTTPError(w, r, http.StatusBadRequest, err.Error())
		return
	}

	response.JSON(w, r, http.StatusOK, response.Map{"schoolyears": schoolyears})
}

func (syr SchoolYearRouter) GetOneHandler(w http.ResponseWriter, r *http.Request) {
	id, ok := getId(w, r)
	if !ok {
		return
	}

	ctx := r.Context()
	sy, err := syr.Repository.GetOne(ctx, id)
	if err != nil {
		response.HTTPError(w, r, http.StatusNotFound, err.Error())
		return
	}

	response.JSON(w, r, http.StatusOK, response.Map{"schoolyear": sy})
}

func (syr SchoolYearRouter) Createandler(w http.ResponseWriter, r *http.Request) {
	sy, ok := decodeSchoolYear(w, r)
	if !ok {
		return
	}

	ctx := r.Context()
	err := syr.Repository.Create(ctx, &sy)
	if err != nil {
		response.HTTPError(w, r, http.StatusBadRequest, err.Error())
	}

	w.Header().Add("Location", fmt.Sprintf("%s%d", r.URL.String(), sy.Id))
	response.JSON(w, r, http.StatusCreated, response.Map{"schoolyear": sy})
}

func decodeSchoolYear(w http.ResponseWriter, r *http.Request) (models.SchoolYear, bool) {
	var sy models.SchoolYear
	err := json.NewDecoder(r.Body).Decode(&sy)
	if err != nil {
		response.HTTPError(w, r, http.StatusBadRequest, err.Error())
		return models.SchoolYear{}, false
	}

	defer r.Body.Close()

	return sy, true
}
