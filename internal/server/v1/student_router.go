package v1

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/go-chi/chi"
	"gitlab.com/softwareperonista/agenda-docente-backend/internal/middleware"
	"gitlab.com/softwareperonista/agenda-docente-backend/internal/models"
	"gitlab.com/softwareperonista/agenda-docente-backend/internal/server/response"
)

type StudentRouter struct {
	Repository models.StudentRepository
}

const StudentPath string = "/students"

func (sr *StudentRouter) Routes() http.Handler {
	r := chi.NewRouter()

	r.With(middleware.Authorizator).Get("/", sr.GetAllHandler)
	r.With(middleware.Authorizator).Get("/{id}", sr.GetOneHandler)
	r.With(middleware.Authorizator).Post("/", sr.CreateHandler)
	r.With(middleware.Authorizator).Put("/", sr.UpdateHandler)
	r.With(middleware.Authorizator).Delete("/", sr.DeleteHandler)

	return r
}

func (sr *StudentRouter) GetAllHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	students, err := sr.Repository.GetAll(ctx)
	if err != nil {
		response.HTTPError(w, r, http.StatusBadRequest, err.Error())
		return
	}

	response.JSON(w, r, http.StatusOK, response.Map{"students": students})
}

func (sr *StudentRouter) GetOneHandler(w http.ResponseWriter, r *http.Request) {
	id, ok := getId(w, r)
	if !ok {
		return
	}

	ctx := r.Context()
	s, err := sr.Repository.GetOne(ctx, id)
	if err != nil {
		response.HTTPError(w, r, http.StatusNotFound, err.Error())
		return
	}

	response.JSON(w, r, http.StatusOK, response.Map{"student": s})
}

func (sr *StudentRouter) CreateHandler(w http.ResponseWriter, r *http.Request) {
	s, ok := decodeStudent(w, r)
	if !ok {
		return
	}

	ctx := r.Context()
	err := sr.Repository.Create(ctx, &s)
	if err != nil {
		response.HTTPError(w, r, http.StatusBadRequest, err.Error())
		return
	}

	w.Header().Add("Location", fmt.Sprintf("%s%d", r.URL.String(), s.Id))
	response.JSON(w, r, http.StatusCreated, response.Map{"student": s})
}

func (sr *StudentRouter) UpdateHandler(w http.ResponseWriter, r *http.Request) {
	id, ok := getId(w, r)
	if !ok {
		return
	}

	s, ok := decodeStudent(w, r)
	if !ok {
		return
	}

	ctx := r.Context()
	err := sr.Repository.Update(ctx, id, s)
	if err != nil {
		response.HTTPError(w, r, http.StatusNotFound, err.Error())
		return
	}

	response.JSON(w, r, http.StatusOK, nil)
}

func (sr *StudentRouter) DeleteHandler(w http.ResponseWriter, r *http.Request) {
	id, ok := getId(w, r)
	if !ok {
		return
	}

	ctx := r.Context()
	err := sr.Repository.Delete(ctx, id)
	if err != nil {
		response.HTTPError(w, r, http.StatusNotFound, err.Error())
		return
	}

	response.JSON(w, r, http.StatusOK, response.Map{})
}

func decodeStudent(w http.ResponseWriter, r *http.Request) (models.Student, bool) {
	var s models.Student
	err := json.NewDecoder(r.Body).Decode(&s)
	if err != nil {
		response.HTTPError(w, r, http.StatusBadRequest, err.Error())
		return models.Student{}, false
	}

	defer r.Body.Close()

	return s, true
}
