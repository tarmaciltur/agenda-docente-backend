package response_test

import (
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/softwareperonista/agenda-docente-backend/internal/server/response"
)

func TestJSON(t *testing.T) {
	r := httptest.NewRequest("GET", "http://example.com/foo", nil)
	w := httptest.NewRecorder()

	response.JSON(w, r, http.StatusOK, nil)

	res := w.Result()

	if res.StatusCode != 200 {
		t.Error("wrong statusCode")
	}

	if res.Header.Get("Content-Type") != "application/json; charset=utf-8" {
		t.Error("Wrong Content-Type")
	}

	body, _ := io.ReadAll(res.Body)

	if string(body) != "" {
		t.Error("body isn't empty")
	}
}

func TestJSONData(t *testing.T) {
	type ColorGroup struct {
		ID     int
		Name   string
		Colors []string
	}
	group := ColorGroup{
		ID:     1,
		Name:   "Reds",
		Colors: []string{"Crimson", "Red", "Ruby", "Maroon"},
	}

	r := httptest.NewRequest("GET", "http://example.com/foo", nil)
	w := httptest.NewRecorder()

	response.JSON(w, r, http.StatusOK, group)

	res := w.Result()
	body, _ := io.ReadAll(res.Body)

	if string(body) != `{"ID":1,"Name":"Reds","Colors":["Crimson","Red","Ruby","Maroon"]}` {
		t.Error("Wrong body")
	}
}

func TestHTTPError(t *testing.T) {
	r := httptest.NewRequest("GET", "http://example.com/foo", nil)
	w := httptest.NewRecorder()

	response.HTTPError(w, r, http.StatusNotFound, "error")

	res := w.Result()
	body, _ := io.ReadAll(res.Body)

	if string(body) != `{"message":"error"}` {
		t.Error("Wrong body")
	}
}
