package database

func Model() string {
	model := `
		CREATE TABLE IF NOT EXISTS users (
    		id INT GENERATED ALWAYS AS IDENTITY,
    		first_name VARCHAR(150) NOT NULL,
    		last_name VARCHAR(150) NOT NULL,
    		username VARCHAR(150) NOT NULL UNIQUE,
    		password varchar(256) NOT NULL,
    		email VARCHAR(150) NOT NULL UNIQUE,
    		picture VARCHAR(256) NOT NULL,
    		CONSTRAINT pk_users PRIMARY KEY(id)
		);

		CREATE TABLE IF NOT EXISTS students (
			id INT GENERATED ALWAYS AS IDENTITY,
    		first_name VARCHAR(150) NOT NULL,
    		last_name VARCHAR(150) NOT NULL,
			gender VARCHAR(30),
			date_of_birth TIMESTAMP,
			CONSTRAINT pk_students PRIMARY KEY(id) 
		);

		CREATE TABLE IF NOT EXISTS positions (
			id INT GENERATED ALWAYS AS IDENTITY,
			year VARCHAR(10) NOT NULL,
			division VARCHAR(50) NOT NULL,
			institution VARCHAR(150) NOT NULL,
			teacher INTEGER NOT NULL,
			CONSTRAINT pk_positon PRIMARY KEY(id),
			CONSTRAINT fk_teacher
				FOREIGN KEY(teacher) 
					REFERENCES users(id)		
		);

		CREATE TABLE IF NOT EXISTS schedules (
			id INT GENERATED ALWAYS AS IDENTITY,
			weekday INTEGER NOT NULL,
			start_time TIMESTAMP NOT NULL,
			end_time TIMESTAMP NOT NULL,
			position INTEGER NOT NULL,
			CONSTRAINT pk_schedule PRIMARY KEY(id),
			CONSTRAINT fk_position
				FOREIGN KEY(position) 
					REFERENCES positions(id)		
		);

		CREATE TABLE IF NOT EXISTS schoolyears (
			id INT GENERATED ALWAYS AS IDENTITY,
			year INT NOT NULL,
			grade VARCHAR(30) NOT NULL,
			first_day TIMESTAMP NOT NULL,
			last_day TIMESTAMP NOT NULL,
			break_start TIMESTAMP,
			break_end TIMESTAMP
		);
	`

	return model
}
