package models

type Course struct {
	Id         uint       `json:"id,omitempty"`
	SchoolYear SchoolYear `json:"schoolyear,omitempty"`
	Position   Position   `json:"position,omitempty"`
}
