package models

import "context"

type PositionRepository interface {
	GetAll(ctx context.Context) ([]Position, error)
	GetOne(ctx context.Context, id uint) (Position, error)
	GetByTeacher(ctx context.Context, tId uint) ([]Position, error)
	Create(ctx context.Context, position *Position) error
	Update(ctx context.Context, id uint, Position Position) error
	Delete(ctx context.Context, id uint) error
}
