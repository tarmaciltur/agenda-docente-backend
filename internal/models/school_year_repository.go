package models

import "context"

type SchoolYearRespository interface {
	GetAll(ctx context.Context) ([]SchoolYear, error)
	GetOne(ctx context.Context, id uint) (SchoolYear, error)
	Create(ctx context.Context, s *SchoolYear) error
	Update(ctx context.Context, id uint, s SchoolYear) error
	Delete(ctx context.Context, id uint) error
}
