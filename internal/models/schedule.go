package models

import "time"

type Schedule struct {
	Id       uint         `json:"id,omitempty"`
	Day      time.Weekday `json:"day,omitempty"`
	Start    time.Time    `json:"start,omitempty"`
	End      time.Time    `json:"end,omitempty"`
	Position uint         `json:"-"`
}
