package models

import "context"

type StudentRepository interface {
	GetAll(ctx context.Context) ([]Student, error)
	GetOne(ctx context.Context, id uint) (Student, error)
	Create(ctx context.Context, student *Student) error
	Update(ctx context.Context, id uint, student Student) error
	Delete(ctx context.Context, id uint) error
}
