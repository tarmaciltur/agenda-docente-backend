package data

import (
	"context"

	"gitlab.com/softwareperonista/agenda-docente-backend/internal/models"
)

type StudentRepository struct {
	Data *Data
}

func (sr *StudentRepository) GetAll(ctx context.Context) ([]models.Student, error) {
	q := `
		SELECT id, first_name, last_name, gender, date_of_birth
			FROM students;
	`

	rows, err := sr.Data.DB.QueryContext(ctx, q)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	var students []models.Student
	for rows.Next() {
		var s models.Student
		rows.Scan(&s.Id, &s.FirstName, &s.LastName, &s.Gender, &s.DateOfBirth)
		students = append(students, s)
	}

	return students, nil
}

func (sr *StudentRepository) GetOne(ctx context.Context, id uint) (models.Student, error) {
	q := `
		SELECT id, first_name, last_name, gender, date_of_birth
			FROM students
			WHERE id = $1;
	`

	row := sr.Data.DB.QueryRowContext(ctx, q, id)

	var s models.Student
	err := row.Scan(&s.Id, &s.FirstName, &s.LastName, &s.Gender, &s.DateOfBirth)
	if err != nil {
		return models.Student{}, err
	}

	return s, nil
}

func (sr *StudentRepository) Create(ctx context.Context, s *models.Student) error {
	q := `
		INSERT INTO students (first_name, last_name, gender, date_of_birth)
			VALUES ($1, $2, $3, $4)
		RETURNING id;
	`

	row := sr.Data.DB.QueryRowContext(ctx, q, s.FirstName, s.LastName, s.Gender, s.DateOfBirth)

	err := row.Scan(&s.Id)
	if err != nil {
		return err
	}

	return nil
}

func (sr *StudentRepository) Update(ctx context.Context, id uint, s models.Student) error {
	q := `
		UPDATE sutdents SET first_name = $1, last_name = $2, gender = $3, date_of_birth = $4
			WHERE id = $5;
	`

	stmt, err := sr.Data.DB.PrepareContext(ctx, q)
	if err != nil {
		return err
	}

	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, s.FirstName, s.LastName, s.Gender, s.DateOfBirth, id)
	if err != nil {
		return err
	}

	return nil
}

func (sr *StudentRepository) Delete(ctx context.Context, id uint) error {
	q := `
		DELETE FROM students WHERE id = $1;
	`

	stmt, err := sr.Data.DB.PrepareContext(ctx, q)
	if err != nil {
		return nil
	}

	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, id)
	if err != nil {
		return err
	}

	return nil
}
