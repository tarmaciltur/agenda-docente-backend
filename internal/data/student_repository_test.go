package data_test

import (
	"log"
	"net/http/httptest"
	"regexp"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"gitlab.com/softwareperonista/agenda-docente-backend/internal/data"
	"gitlab.com/softwareperonista/agenda-docente-backend/internal/models"
)

func createStudentRepository() (data.StudentRepository, sqlmock.Sqlmock) {
	db, mock, err := sqlmock.New()
	if err != nil {
		log.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}

	return data.StudentRepository{Data: &data.Data{DB: db}}, mock
}

func TestStudentGetAll(t *testing.T) {
	sr, mock := createStudentRepository()

	mock.ExpectQuery(`SELECT id, first_name, last_name, gender, date_of_birth FROM students;`).
		WillReturnRows(sqlmock.NewRows([]string{"id", "first_name", "last_name", "gender", "date_of_birth"}).
			AddRow(1, "First Name", "Last Name", "Male", time.Date(1970, time.January, 01, 0, 0, 0, 0, time.UTC)).
			AddRow(2, "First Name 2", "Last Name 2", "Female", time.Date(1997, time.October, 10, 0, 0, 0, 0, time.UTC)))

	r := httptest.NewRequest("GET", "http://example.com/foo", nil)

	ctx := r.Context()
	students, err := sr.GetAll(ctx)
	if err != nil {
		t.Error(err.Error())
	}

	if len(students) != 2 {
		t.Error("len(students): %user. Should be 2", len(students))
	}

	if students[0].Id != 1 ||
		students[0].FirstName != "First Name" ||
		students[0].LastName != "Last Name" ||
		students[0].Gender != "Male" ||
		students[0].DateOfBirth != time.Date(1970, time.January, 01, 0, 0, 0, 0, time.UTC) {
		t.Error("Wrong student[0] data")
	}

	if students[1].Id != 2 ||
		students[1].FirstName != "First Name 2" ||
		students[1].LastName != "Last Name 2" ||
		students[1].Gender != "Female" ||
		students[1].DateOfBirth != time.Date(1997, time.October, 10, 0, 0, 0, 0, time.UTC) {
		t.Error("Wrong student[1] data")
	}
}

func TestStudentGetOne(t *testing.T) {
	sr, mock := createStudentRepository()

	mock.ExpectQuery(regexp.QuoteMeta(`SELECT id, first_name, last_name, gender, date_of_birth FROM students WHERE id = $1;`)).
		WillReturnRows(sqlmock.NewRows([]string{"id", "first_name", "last_name", "gender", "date_of_birth"}).
			AddRow(1, "First Name", "Last Name", "Male", time.Date(1970, time.January, 01, 0, 0, 0, 0, time.UTC)))

	r := httptest.NewRequest("GET", "http://example.com/foo", nil)

	ctx := r.Context()
	student, err := sr.GetOne(ctx, 1)
	if err != nil {
		t.Error(err.Error())
	}

	if student.Id != 1 ||
		student.FirstName != "First Name" ||
		student.LastName != "Last Name" ||
		student.Gender != "Male" ||
		student.DateOfBirth != time.Date(1970, time.January, 01, 0, 0, 0, 0, time.UTC) {
		t.Error("Wrong student data")
	}
}

func TestStudentCreate(t *testing.T) {
	sr, mock := createStudentRepository()

	mock.ExpectQuery(regexp.QuoteMeta(`INSERT INTO students (first_name, last_name, gender, date_of_birth) VALUES ($1, $2, $3, $4) RETURNING id;`)).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).
			AddRow(1))

	r := httptest.NewRequest("GET", "http://example.com/foo", nil)

	student := models.Student{
		FirstName:   "First Name",
		LastName:    "Last Name",
		Gender:      "Male",
		DateOfBirth: time.Date(1970, time.January, 01, 0, 0, 0, 0, time.UTC),
	}

	ctx := r.Context()
	err := sr.Create(ctx, &student)
	if err != nil {
		t.Error(err.Error())
	}
}

func TestStudentUpdate(t *testing.T) {
	sr, mock := createStudentRepository()

	mock.ExpectPrepare(regexp.QuoteMeta(`UPDATE sutdents SET first_name = $1, last_name = $2, gender = $3, date_of_birth = $4 WHERE id = $5;`)).
		WillBeClosed().ExpectExec().WithArgs("First Name", "Last Name", "Male", time.Date(1970, time.January, 1, 0, 0, 0, 0, time.UTC), 1).WillReturnResult(sqlmock.NewResult(1, 1))

	r := httptest.NewRequest("GET", "http://example.com/foo", nil)

	student := models.Student{
		FirstName:   "First Name",
		LastName:    "Last Name",
		Gender:      "Male",
		DateOfBirth: time.Date(1970, time.January, 01, 0, 0, 0, 0, time.UTC),
	}

	ctx := r.Context()
	err := sr.Update(ctx, uint(1), student)
	if err != nil {
		t.Error(err.Error())
	}

	err = mock.ExpectationsWereMet()
	if err != nil {
		t.Error(err.Error())
	}
}

func TestStudentDelete(t *testing.T) {
	sr, mock := createStudentRepository()

	mock.ExpectPrepare(regexp.QuoteMeta(`DELETE FROM students WHERE id = $1;`)).
		WillBeClosed().ExpectExec().WithArgs(1).WillReturnResult(sqlmock.NewResult(1, 1))

	r := httptest.NewRequest("GET", "http://example.com/foo", nil)

	ctx := r.Context()
	err := sr.Delete(ctx, uint(1))
	if err != nil {
		t.Error(err.Error())
	}

	err = mock.ExpectationsWereMet()
	if err != nil {
		t.Error(err.Error())
	}
}
