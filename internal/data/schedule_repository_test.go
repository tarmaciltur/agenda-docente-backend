package data_test

import (
	"errors"
	"log"
	"net/http/httptest"
	"regexp"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"gitlab.com/softwareperonista/agenda-docente-backend/internal/data"
	"gitlab.com/softwareperonista/agenda-docente-backend/internal/models"
)

func createScheduleRepository() (data.ScheduleRepository, sqlmock.Sqlmock) {
	db, mock, err := sqlmock.New()
	if err != nil {
		log.Fatalf("an error '%s' was not expected when opening a stub database connection", err.Error())
	}

	return data.ScheduleRepository{Data: &data.Data{DB: db}}, mock
}

func TestScheduleGetByPosition(t *testing.T) {
	sr, mock := createScheduleRepository()

	mock.ExpectQuery(regexp.QuoteMeta(`SELECT id, weekday, start_time, end_time, position FROM schedules WHERE position = $1;`)).
		WillReturnRows(sqlmock.NewRows([]string{"id", "weekday", "start_time", "end_time", "position"}).
			AddRow(1, 3, time.Date(1997, time.October, 10, 9, 30, 0, 0, time.UTC), time.Date(1997, time.October, 10, 11, 30, 0, 0, time.UTC), 1).
			AddRow(2, 2, time.Date(1997, time.October, 10, 7, 0, 0, 0, time.UTC), time.Date(1997, time.October, 10, 10, 0, 0, 0, time.UTC), 1))

	r := httptest.NewRequest("GET", "http://example.com/foo", nil)

	ctx := r.Context()
	schedules, err := sr.GetByPosition(ctx, 1)
	if err != nil {
		t.Error(err.Error())
	}

	if len(schedules) != 2 {
		t.Errorf("wrong len(schedules). Expected %d, got %d", len(schedules), 2)
	}

	if schedules[0].Id != 1 ||
		schedules[0].Day != 3 ||
		schedules[0].Start != time.Date(1997, time.October, 10, 9, 30, 0, 0, time.UTC) ||
		schedules[0].End != time.Date(1997, time.October, 10, 11, 30, 0, 0, time.UTC) ||
		schedules[0].Position != 1 {
		t.Error("wrong schedule[0] data")
	}

	if schedules[1].Id != 2 ||
		schedules[1].Day != 2 ||
		schedules[1].Start != time.Date(1997, time.October, 10, 7, 0, 0, 0, time.UTC) ||
		schedules[1].End != time.Date(1997, time.October, 10, 10, 0, 0, 0, time.UTC) ||
		schedules[1].Position != 1 {
		t.Error("wrong schedule[1] data")
	}

	t.Run("wrong position", func(t *testing.T) {
		sr, mock := createScheduleRepository()

		mock.ExpectQuery(regexp.QuoteMeta(`SELECT id, weekday, start_time, end_time, position FROM schedules WHERE position = $1;`)).
			WillReturnError(errors.New(""))

		r := httptest.NewRequest("GET", "http://example.com/foo", nil)

		ctx := r.Context()
		_, err := sr.GetByPosition(ctx, 1)
		if err == nil {
			t.Error("wrong position id should return error")
		}
	})
}

func TestScheduleCreate(t *testing.T) {
	sr, mock := createScheduleRepository()

	mock.ExpectBegin()
	mock.ExpectQuery(regexp.QuoteMeta(`INSERT INTO schedules (weekday, start_time, end_time, position) VALUES ($1, $2, $3, $4) RETURNING id;`)).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).
			AddRow(1))

	r := httptest.NewRequest("GET", "http://example.com/foo", nil)

	schedule := models.Schedule{
		Id:       1,
		Day:      3,
		Start:    time.Date(1997, time.October, 10, 9, 30, 0, 0, time.UTC),
		End:      time.Date(1997, time.October, 10, 11, 30, 0, 0, time.UTC),
		Position: 1,
	}

	ctx := r.Context()
	tx, err := sr.Data.DB.BeginTx(ctx, nil)
	if err != nil {
		t.Error(err.Error())
	}

	err = sr.Create(ctx, tx, &schedule)
	if err != nil {
		t.Error(err.Error())
	}

	t.Run("error creating", func(t *testing.T) {
		sr, mock := createScheduleRepository()

		mock.ExpectBegin()
		mock.ExpectQuery(regexp.QuoteMeta(`INSERT INTO schedules (weekday, start_time, end_time, position) VALUES ($1, $2, $3, $4) RETURNING id;`)).
			WillReturnError(errors.New(""))

		r := httptest.NewRequest("GET", "http://example.com/foo", nil)

		ctx := r.Context()
		tx, err := sr.Data.DB.BeginTx(ctx, nil)
		if err != nil {
			t.Error(err.Error())
		}

		err = sr.Create(ctx, tx, &schedule)
		if err == nil {
			t.Error("insert failed should return error")
		}
	})
}

func TestScheduleUpdate(t *testing.T) {
	sr, mock := createScheduleRepository()

	schedule := models.Schedule{
		Id:       1,
		Day:      3,
		Start:    time.Date(1997, time.October, 10, 9, 30, 0, 0, time.UTC),
		End:      time.Date(1997, time.October, 10, 11, 30, 0, 0, time.UTC),
		Position: 1,
	}

	mock.ExpectBegin()
	mock.ExpectPrepare(regexp.QuoteMeta(`UPDATE schedules SET weekday = $1, start_time = $2, end_time = $3, position = $4 WHERE id = $5;`)).
		WillBeClosed().ExpectExec().WithArgs(schedule.Day, schedule.Start, schedule.End, schedule.Position, 1).WillReturnResult(sqlmock.NewResult(1, 1))

	r := httptest.NewRequest("GET", "http://example.com/foo", nil)

	ctx := r.Context()
	tx, err := sr.Data.DB.BeginTx(ctx, nil)
	if err != nil {
		t.Error(err.Error())
	}

	err = sr.Update(ctx, tx, 1, schedule)
	if err != nil {
		t.Error(err.Error())
	}

	t.Run("wrong id", func(t *testing.T) {
		sr, mock := createScheduleRepository()

		mock.ExpectBegin()
		mock.ExpectPrepare(regexp.QuoteMeta(`UPDATE schedules SET weekday = $1, start_time = $2, end_time = $3, position = $4 WHERE id = $5;`)).
			WillBeClosed().ExpectExec().WillReturnError(errors.New(""))

		r := httptest.NewRequest("GET", "http://example.com/foo", nil)

		ctx := r.Context()
		tx, err := sr.Data.DB.BeginTx(ctx, nil)
		if err != nil {
			t.Error(err.Error())
		}

		err = sr.Update(ctx, tx, 1, schedule)
		if err == nil {
			t.Error("db exec fail should return error")
		}
	})
}

func TestScheduleDelete(t *testing.T) {
	sr, mock := createScheduleRepository()

	mock.ExpectBegin()
	mock.ExpectPrepare(regexp.QuoteMeta(`DELETE FROM shedules WHERE id = $1;`)).
		WillBeClosed().ExpectExec().WithArgs(1).WillReturnResult(sqlmock.NewResult(1, 1))

	r := httptest.NewRequest("GET", "http://example.com/foo", nil)

	ctx := r.Context()
	tx, err := sr.Data.DB.BeginTx(ctx, nil)
	if err != nil {
		t.Error(err.Error())
	}

	err = sr.Delete(ctx, tx, 1)
	if err != nil {
		t.Error(err.Error())
	}

	t.Run("wrong id", func(t *testing.T) {
		sr, mock := createScheduleRepository()

		mock.ExpectBegin()
		mock.ExpectPrepare(regexp.QuoteMeta(`UPDATE schedules SET weekday = $1, start_time = $2, end_time = $3, position = $4 WHERE id = $5;`)).
			WillBeClosed().ExpectExec().WillReturnError(errors.New(""))

		r := httptest.NewRequest("GET", "http://example.com/foo", nil)

		ctx := r.Context()
		tx, err := sr.Data.DB.BeginTx(ctx, nil)
		if err != nil {
			t.Error(err.Error())
		}

		err = sr.Delete(ctx, tx, 1)
		if err == nil {
			t.Error("db exec fail should return error")
		}
	})
}
